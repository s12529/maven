package hyperdemo.hyperdemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class PersonManager {

	Connection connection;
	
	private String url = "jdbc:hsqldb:hsql://localhost/workdb";
	
	private String createTable = "CREATE TABLE Person(id bigint GENERATED BY DEFAULT AS IDENTITY, "
			+ "name varchar(20), "
			+ "yob integer)";
	
	public PersonManager() {
		try {
			connection = DriverManager.getConnection(url);
			Statement stmt = connection.createStatement();
			stmt.executeUpdate(createTable);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
